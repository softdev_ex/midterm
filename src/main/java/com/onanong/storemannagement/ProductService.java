/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onanong.storemannagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author onanongchanwiset
 */
public class ProductService {

    private static ArrayList<Product> productList = new ArrayList<>();

    static {
//        productList = new ArrayList<>();
//        // Mock data
//        productList.add(new Product("Rice", "easy", 15.0, 1));
//        productList.add(new Product("Sausage", "Ceepee", 35.0, 1));

    }

    // Create
    public static boolean addProduct(Product product) {
        productList.add(product);
        save();
        return true;
    }

    // Get All
    public static ArrayList<Product> getProduct() {
        return productList;
    }

    // Get Index
    public static Product getProduct(int index) {
        return productList.get(index);
    }

    // Update
    public static boolean updateProduct(int index, Product product) {
        productList.set(index, product);
        save();
        return true;
    }

    // Delete
    public static boolean delProduct(int index) {
        productList.remove(index);
        save();
        return true;
    }

    public static boolean delProductAll() {
        productList.removeAll(productList);
        save();
        return true;
    }
    
    //File
    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("oom.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(productList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("oom.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            productList = (ArrayList<Product>)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
