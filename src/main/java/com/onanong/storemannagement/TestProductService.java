/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onanong.storemannagement;

/**
 *
 * @author onanongchanwiset
 */
public class TestProductService {

    public static void main(String[] args) {
        System.out.println(ProductService.getProduct());
        ProductService.addProduct(new Product("Sausage", "ArhanYodkhun", 37.0, 2));
        System.out.println(ProductService.getProduct());
        Product updateProduct = new Product("Snack", "Ceepee", 35.0, 1);
        ProductService.updateProduct(1, updateProduct);
        System.out.println(ProductService.getProduct());
        ProductService.delProduct(2);
        System.out.println(ProductService.getProduct());
    }
}
